
package palindrometester;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Christones Michel 3613792
 * Programming II
 * Prof. Charters
 */
public class PalindromeTester 
{

    /**
     * @param args the command line arguments
     */
    static LinkedList<String> temp;
    public static void main(String[] args) 
    {
        File myFile;
        Scanner inFile = null;
        String word;
        LinkedList<String> possiblePal;
        HelperPal myPalHelp = new HelperPal();
        
        try
        {
            myFile = new File("candidates.txt");
            inFile = new Scanner(myFile);
            System.out.println("Program to check if a word is a palindrome or not"
                    + "\n------------------------------------------------");
            while (inFile.hasNext())
            {
                word = inFile.next();
                possiblePal = makeLinkedList(word);
                myPalHelp.setReverse(possiblePal);
                
                if (myPalHelp.isPalindrome(possiblePal))
                {
                    System.out.println(word + " is a Palindrome.");
                    
                }
                else
                {
                    System.out.println(word + " is NOT a Palindrome.");
                }
        
            }
      
        }
        catch (IOException e)
        {
            System.out.println("Sorry, wrong file.  Come back later.");
        }
        finally
        {
            if (inFile != null)
                inFile.close();
        }
        
    }
    
    public static LinkedList<String> makeLinkedList(String word)
    {
        //substringing every letter, every letter will be a node on the list
        LinkedList<String> hold = new LinkedList<>();
        for(int i =0; i< word.length(); i++)
        {
            hold.add(Character.toString(word.charAt(i)));
        }
        return hold;
    }
    
}
