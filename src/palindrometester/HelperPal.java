
package palindrometester;

import java.util.LinkedList;

/**
 *
 * @author Christones Michel 3613792
 */
public class HelperPal {
LinkedList<String> reverse;

    public void setReverse(LinkedList<String> aLL) {
        this.reverse = reverseAndClone(aLL);
        //System.out.println(reverse);
    }

    public boolean isPalindrome(LinkedList<String> aLL) {
        if (isEqual(aLL, reverse)) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "HelperPal{" + "reverse=" + reverse + '}';
    }
    
    /**
     * 
     * @param inputLL
     * @return The reverse of the linked list
     */
    public LinkedList<String> reverseAndClone(LinkedList<String> inputLL) {
       

        LinkedList<String> reverse = new LinkedList<>();

        for (int i = inputLL.size()-1; i >= 0; i--) {

            reverse.add(inputLL.get(i));

        }

        return reverse;
    }

    /**
     * 
     * @param aLL1 Takes linkedlist 1
     * @param aLL2 Takes  linkedlist 2
     * @return return if its a match
     */
    public boolean isEqual(LinkedList<String> aLL1, LinkedList<String> aLL2) {
        //how to traverse both at same time
        //iterator per linkedlist //.equallsignorecase /while loop
        for(int i =0; i < aLL1.size();i++)
        {
            
            if(!aLL1.get(i).equalsIgnoreCase(aLL2.get(i))) return false;
        }
        
        return true;
    }
    
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }


}
